<?php

namespace App\Form;

use App\Entity\Ingredient;
use App\Entity\Recipe;
use App\Repository\IngredientRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class,[
                'attr'=>[
                ],
                'label' => 'Nom',
                'constraints' =>[
                ]
            ])
            ->add('time', IntegerType::class,[
                'attr'=>[
                ],
                'label' => 'Temps en minutes',
                'constraints' =>[
                ]
            ])
            ->add('nbPeople', IntegerType::class,[
                'attr'=>[
                ],
                'label' => 'Nombre de personne',
                'constraints' =>[
                ]
            ])
            ->add('difficulty', RangeType::class,[
                'attr'=>[
                ],
                'label' => 'Difficulté (1 à 5)',
                'constraints' =>[
                ]
            ])
            ->add('description', TextType::class,[
                'attr'=>[
                ],
                'label' => 'Description',
                'constraints' =>[
                ]
            ])
            ->add('price', IntegerType::class,[
                'attr'=>[
                ],
                'label' => 'Prix',
                'constraints' =>[
                ]
            ])
            ->add('isFavorite', CheckboxType::class,[
                'attr'=>[
                ],
                'required'=>false,
                'label' => 'Ajouter aux favoris',
                'constraints' =>[
                ]
            ])
            ->add('ingredients', EntityType::class, [

                'class' => Ingredient::class,
                'query_builder' => function (IngredientRepository $repository){
                    return $repository->createQueryBuilder('i')
                        ->orderBy('i.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => 'true',
                'expanded'=>'true',
                'attr' => [
                ]
            ])
            ->add('submit', SubmitType::class,[
                'attr' => [
                    'class' => 'btn btn-primary'
                ],
                'label' => 'Appliquer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recipe::class,
        ]);
    }
}
