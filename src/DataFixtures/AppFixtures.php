<?php

namespace App\DataFixtures;

use App\Entity\Ingredient;
use App\Entity\Recipe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        //ingrédients
        $ingredients=[];
        for ($i = 1; $i <= 20; $i++){
            $ingredient = new Ingredient();
            $ingredient
                ->setName('Ingrédient '.$i)
                ->setPrice(mt_rand(2, 100));

            $ingredients[] = $ingredient;
            $manager->persist($ingredient);
        }


/*        //Recipes
        $recipes=[];
        for ($j = 0; $j <= 25; $j++){
            $recipe = new Recipe();
            $recipe
                ->setName('Recette '.$j)
                ->setTime(mt_rand(0, 60))
                ->setNbPeople(mt_rand(0, 1) == 1 ? mt_rand(1, 50) : null)
                ->setDifficulty(mt_rand(0, 1) == 1 ? mt_rand(1, 5) : null)
                ->setDescription('Une description de qualité pour la recette '.$i)
                ->setPrice(mt_rand(1, 1000))
                ->setIsFavorite(mt_rand(0, 1) == 1 ? true: false);

            for ($k = 0; $k <= mt_rand(5,15); $k++){
                $recipe->addIngredient($ingredient[mt_rand(0, count($ingredients) - 1 )]);
            }

            $manager->persist($recipe);
        }*/

        $manager->flush();
    }
}
